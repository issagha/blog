---
title: A propos
subtitle: Quelques informations 
comments: false
---

## Pôle informatique - Université d'Orléans


Bâtiment IIIA

Rue Léonard de Vinci

B.P. 6759

45067 ORLEANS Cedex 2


### Site web officiel

https://www.univ-orleans.fr/fr/sciences-techniques/formation/informatique

