---
title: "Hugo en Asciidoc ?"
date: 2020-12-28
authors: ["Fred"]
draft: false
summary: Article en asciidoc pour tester la configuration et le déploiement
tags: ["asciidoc", "hugo", "gitlab"]
---

= Hugo en Asciidoc ?
:source-highlighter: highlightjs

Tests des rendus d'éléments en Asciidoc.


== Un diagramme UML

Un diagramme de classe du pattern Décorateur pour vérifier la génération des images.

[plantuml, decorator, svg]
----
title le pattern Décorateur


interface Composant {
{abstract} method1()
}

together {
class ComposantConcretA
class ComposantConcretB
}

ComposantConcretA : method1()
ComposantConcretB : method1()

class Decorateur {
Composant decoré
method1()
method2()
}
class DecorateurConcret1 {
}
together {
class Decorateur
class DecorateurConcret1
}

Decorateur -> Composant
Composant <|-- ComposantConcretA
Composant <|-- ComposantConcretB
Composant <|-- Decorateur
Decorateur <|-- DecorateurConcret1
----




== Du code Java

Même chose avec du code Java.

[source,java]
----
public class Demo {
    private int age;

    public static void main(String[] args){
            int i = 5;
    }
}
----


== Et les images ?

image::asciidoctor.png[asciidoctor]